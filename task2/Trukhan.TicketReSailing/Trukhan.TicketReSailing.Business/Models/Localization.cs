﻿
using System.Collections.Generic;

namespace Trukhan.TicketReSailing.Business.Models
{
    public class Localization
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; } //en-US, ru-Ru, be-BY
        public List<User> Users { get; set; }
    }
}
