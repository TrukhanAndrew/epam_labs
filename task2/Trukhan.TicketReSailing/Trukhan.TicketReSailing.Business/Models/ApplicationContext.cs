﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Trukhan.TicketReSailing.Business.Models
{
    public class ApplicationContext: IdentityDbContext<User>
    {
        public DbSet<Localization> Localizations { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Order> Orders  { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Venue> Venues  { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }
    }
}
