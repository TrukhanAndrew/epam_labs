﻿using System;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Localization;

namespace Trukhan.TicketReSailing.Business.Models
{
    public static class CookiesCulture
    {
        public static void SetCookiesCulture(HttpResponse response, string culture) 
        {
            culture = (culture != null) ? culture : "en";
            response.Cookies.Append(CookieRequestCultureProvider.DefaultCookieName,
                                    CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                                    new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1)});
        }
    }
}
