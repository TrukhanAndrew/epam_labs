﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Trukhan.TicketReSailing.Business.Models
{
    public static class StatusOrder
    {
        public const string Waiting_Confirmation = "waiting confirmation";
        public const string Confirmed = "confirmed";
        public const string Rejected = "rejected ";
    }

    public class Order
    {
        public int Id { get; set; }
        public Ticket Ticket { get; set; }
        public string Status { get; set; }
        public User Buyer { get; set; }
        public string TrakNO { get; set; }
        public string MessageReject { get; set; }
    }
}
