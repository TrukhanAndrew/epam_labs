﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Trukhan.TicketReSailing.Business.Models
{
    public class Event
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        public Venue Venue { get; set; }
        [Required]
        public string Banner { get; set; }
        [Required]
        public string Description { get; set; }
        public List<Ticket> Tickets { get; set; }
    }
}
