﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Trukhan.TicketReSailing.Business.Models
{
    public class Login
    {
        public string UserId { get; private set; }

        [Required(ErrorMessage = "E-mail isn't specified")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Password isn't specified")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public Login()
        {
        }

        public Login(User user, string name, string password)
        {
            UserId = user.Id;
            Name = name;
            Password = password;
        }

        public void ChangeLogin(User user, string newName, string newPassword)
        { }
    }
}
