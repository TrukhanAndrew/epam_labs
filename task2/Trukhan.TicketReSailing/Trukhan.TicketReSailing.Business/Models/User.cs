﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;

namespace Trukhan.TicketReSailing.Business.Models
{
    public class User : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Adress { get; set; }
        public Localization Localization { get; set; }
        public int LocalizationId { get ; set;}
        public List<Ticket> Tickets { get; set; }
        public List<Order> Orders { get; set; }

        [NotMapped]
        public CultureInfo Culture
        {
            get { return new CultureInfo(Localization.Value); }
        }
    }
}
