﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Trukhan.TicketReSailing.Business.Models
{
    public static class StatusTicket
    {
        public const string Waiting_Confirmation = "waiting confirmation";
        public const string Waiting_Payment = "waiting payment";
        public const string Selling = "selling";
        public const string Sold = "sold";
    }

    public class Ticket
    {
        public int Id { get; set; }
        public Event Event { get; set; }
        public double Price { get; set; }
        public User Seller { get; set; }
        public string Status { get; set; }
    }
}
