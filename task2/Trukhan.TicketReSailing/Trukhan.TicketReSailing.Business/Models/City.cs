﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Trukhan.TicketReSailing.Business.Models
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Venue> Venues { get; set; }
    }

    
}
