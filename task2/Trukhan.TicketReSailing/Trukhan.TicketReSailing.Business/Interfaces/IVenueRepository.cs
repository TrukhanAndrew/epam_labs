﻿using System;
using System.Collections.Generic;
using Trukhan.TicketReSailing.Business.Models;


namespace Trukhan.TicketReSailing.Business.Interfaces
{
    public interface IVenueRepository
    {
        void Add (Venue venue);
        void Delete(Venue venue);
        void Update(Venue venue);
        List<Venue> GetAll();
        List<Venue> FindByCityID(int cityID);
        Venue FindById(int venueId);
        Venue FindByName(string venueName);        
    }
}
