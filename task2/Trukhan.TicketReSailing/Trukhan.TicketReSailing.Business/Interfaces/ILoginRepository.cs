﻿using System;
using System.Collections.Generic;
using Trukhan.TicketReSailing.Business.Models;


namespace Trukhan.TicketReSailing.Business.Interfaces
{
    public interface ILoginRepository
    {
        void Add(Login login);
        List<Login> GetAll();
        Login Find(string nameValue);
        Login Find(string nameValue, string passwordValue);
    }
}
