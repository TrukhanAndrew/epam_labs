﻿using System;
using System.Collections.Generic;
using Trukhan.TicketReSailing.Business.Models;


namespace Trukhan.TicketReSailing.Business.Interfaces
{
   public interface IEventRepository
    {
        void Add (Event ev);
        void Delete(Event ev);
        void Update(Event ev);
        List<Event> GetAll();
        Event FindById(int Id);
    }
}
