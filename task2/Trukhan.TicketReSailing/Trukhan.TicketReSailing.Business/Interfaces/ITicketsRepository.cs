﻿using System;
using System.Collections.Generic;
using Trukhan.TicketReSailing.Business.Models;


namespace Trukhan.TicketReSailing.Business.Interfaces
{
    public interface ITicketsRepository
    {
        void Add (Ticket ticket);
        List<Ticket> GetAll();
        Ticket FindByID(int ticketId);
        List<Ticket> FindBySellerName(string name);
        List<Ticket> FindByEventID(int eventId);
        void Update(Ticket ticket);
    }
}
