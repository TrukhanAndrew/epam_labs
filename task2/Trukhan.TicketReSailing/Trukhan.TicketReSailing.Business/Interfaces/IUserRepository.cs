﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Trukhan.TicketReSailing.Business.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Trukhan.TicketReSailing.Business.Interfaces
{
    public interface IUserRepository
    {
        Task<IdentityResult> AddUsersAsync(User user, string password, string role);
        List<User> GetAllUsers();
        List<IdentityRole> GetAllRoles();
        Task<User> FindByUserNameAsync (string userName);
        Task<User> FindByUserIdAsync(string userId);
        bool AddToRole(User user, string role);
        bool IsInRole(User user, string role);
        bool RemoveRole(User user, string role);
    }
}
