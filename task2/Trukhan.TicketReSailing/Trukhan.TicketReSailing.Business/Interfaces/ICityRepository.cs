﻿using System;
using System.Collections.Generic;
using Trukhan.TicketReSailing.Business.Models;


namespace Trukhan.TicketReSailing.Business.Interfaces
{
    public interface ICityRepository
    {
        void Add (City ticket);
        void Delete(City city);
        void Update(City city);
        List<City> GetAll();
        City FindById(int cityId);
        City FindByName(string cityName);
    }
}
