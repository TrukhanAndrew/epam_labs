﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Trukhan.TicketReSailing.Business.Models;


namespace Trukhan.TicketReSailing.Business.Interfaces
{
    public interface ILocalizationRepository
    {
        void Add(Localization localization);
        Localization FindByName (string name);
        List<string> GetLocalizationsName();
        Task<Localization> FindByIdAsync(int localizationID);
    }
}
