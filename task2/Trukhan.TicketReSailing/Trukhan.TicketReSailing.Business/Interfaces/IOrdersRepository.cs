﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Trukhan.TicketReSailing.Business.Models;


namespace Trukhan.TicketReSailing.Business.Interfaces
{
    public interface IOrdersRepository
    {
        void Add(Order order);
        void Update(Order order);
        List<Order> GetAll();
        Order FindByID (int orderId);
        List<Order> FindByBuyerID(string buyerId);
        void CreateNewOrder(Ticket ticket, string userName);
    }
}
