﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Linq;
using System.Threading.Tasks;
using Trukhan.TicketReSailing.Business.Interfaces;
using Trukhan.TicketReSailing.Business.Models;

namespace Trukhan.TicketReSailing.Business.Repositories
{
    public class OrdersRepository : IOrdersRepository
    {
        private ApplicationContext _db;
        private readonly ITicketsRepository _tickets;
        private readonly IUserRepository _users;

        public OrdersRepository(ApplicationContext db, ITicketsRepository tickets, IUserRepository users)
        {
            _db = db;
            _tickets = tickets;
            _users = users;
            AddOrdersIfNoneExist();
        }

        private void AddOrdersIfNoneExist()
        {
            if (_db.Orders.Count() == 0)
            {
                int countTickets = _tickets.GetAll().Count();
                int countUsers = _users.GetAllUsers().Count();
                List<int> indicesOfTickets = new List<int>();

                Ticket ticket = GetCurrentTicket(indicesOfTickets, countTickets);
                User buyer = GetCurrentBuyer(countUsers);

                ticket.Status = StatusTicket.Waiting_Confirmation;
                _db.Tickets.Update(ticket);
                _db.SaveChanges();

                string status = StatusOrder.Waiting_Confirmation;
                string trackNO = "";
                Order order = new Order { Ticket = ticket, Status = status, Buyer = buyer, TrakNO = trackNO };
                Add(order);

                ticket = GetCurrentTicket(indicesOfTickets, countTickets);
                buyer = GetCurrentBuyer(countUsers);
                status = StatusOrder.Confirmed;
                ticket.Status = StatusTicket.Sold;
                _db.Tickets.Update(ticket);
                _db.SaveChanges();
                trackNO = "EF22232489";
                order = new Order { Ticket = ticket, Status = status, Buyer = buyer, TrakNO = trackNO };
                Add(order);

                ticket = GetCurrentTicket(indicesOfTickets, countTickets);
                buyer = GetCurrentBuyer(countUsers);
                status = StatusOrder.Confirmed;
                ticket.Status = StatusTicket.Sold;
                _db.Tickets.Update(ticket);
                _db.SaveChanges();
                trackNO = "EF22255555";
                order = new Order { Ticket = ticket, Status = status, Buyer = buyer, TrakNO = trackNO };
                Add(order);

                ticket = GetCurrentTicket(indicesOfTickets, countTickets);
                buyer = GetCurrentBuyer(countUsers);
                status = StatusOrder.Rejected;
                ticket.Status = StatusTicket.Selling;
                _db.Tickets.Update(ticket);
                _db.SaveChanges();
                order = new Order { Ticket = ticket, Status = status, Buyer = buyer, TrakNO = trackNO };
                Add(order);

                ticket = GetCurrentTicket(indicesOfTickets, countTickets);
                buyer = GetCurrentBuyer(countUsers);
                status = StatusOrder.Confirmed;
                ticket.Status = StatusTicket.Sold;
                _db.Tickets.Update(ticket);
                _db.SaveChanges();
                trackNO = "EF22230009";
                order = new Order { Ticket = ticket, Status = status, Buyer = buyer, TrakNO = trackNO };
                Add(order);

                ticket = GetCurrentTicket(indicesOfTickets, countTickets);
                buyer = GetCurrentBuyer(countUsers);
                status = StatusOrder.Waiting_Confirmation;
                ticket.Status = StatusTicket.Waiting_Confirmation;
                _db.Tickets.Update(ticket);
                _db.SaveChanges();
                order = new Order { Ticket = ticket, Status = status, Buyer = buyer, TrakNO = trackNO };
                Add(order);

                ticket = GetCurrentTicket(indicesOfTickets, countTickets);
                buyer = GetCurrentBuyer(countUsers);
                status = StatusOrder.Waiting_Confirmation;
                ticket.Status = StatusTicket.Waiting_Confirmation;
                _db.Tickets.Update(ticket);
                _db.SaveChanges();
                order = new Order { Ticket = ticket, Status = status, Buyer = buyer, TrakNO = trackNO };
                Add(order);

                ticket = GetCurrentTicket(indicesOfTickets, countTickets);
                buyer = GetCurrentBuyer(countUsers);
                status = StatusOrder.Confirmed;
                ticket.Status = StatusTicket.Sold;
                _db.Tickets.Update(ticket);
                _db.SaveChanges();
                trackNO = "DC22232489";
                order = new Order { Ticket = ticket, Status = status, Buyer = buyer, TrakNO = trackNO };
                Add(order);

                ticket = GetCurrentTicket(indicesOfTickets, countTickets);
                buyer = GetCurrentBuyer(countUsers);
                status = StatusOrder.Rejected;
                ticket.Status = StatusTicket.Selling;
                _db.Tickets.Update(ticket);
                _db.SaveChanges();
                order = new Order { Ticket = ticket, Status = status, Buyer = buyer, TrakNO = trackNO };
                Add(order);

                ticket = GetCurrentTicket(indicesOfTickets, countTickets);
                buyer = GetCurrentBuyer(countUsers);
                status = StatusOrder.Waiting_Confirmation;
                ticket.Status = StatusTicket.Waiting_Confirmation;
                _db.Tickets.Update(ticket);
                _db.SaveChanges();
                order = new Order { Ticket = ticket, Status = status, Buyer = buyer, TrakNO = trackNO };
                Add(order);

                ticket = GetCurrentTicket(indicesOfTickets, countTickets);
                buyer = GetCurrentBuyer(countUsers);
                status = StatusOrder.Rejected;
                ticket.Status = StatusTicket.Selling;
                _db.Tickets.Update(ticket);
                _db.SaveChanges();
                order = new Order { Ticket = ticket, Status = status, Buyer = buyer, TrakNO = trackNO };
                Add(order);

                ticket = GetCurrentTicket(indicesOfTickets, countTickets);
                buyer = GetCurrentBuyer(countUsers);
                status = StatusOrder.Confirmed;
                ticket.Status = StatusTicket.Sold;
                _db.Tickets.Update(ticket);
                _db.SaveChanges();
                trackNO = "GT00000089";
                order = new Order { Ticket = ticket, Status = status, Buyer = buyer, TrakNO = trackNO };
                Add(order);
            }
        }

        private Ticket GetCurrentTicket(List<int> indicesOfTickets, int countTickets)
        {
            Random indexRandomTicket = new Random();
            int indexCurrentTicket = indexRandomTicket.Next(0, countTickets);
            while (indicesOfTickets.Contains(indexCurrentTicket))
            {
                indexCurrentTicket = indexRandomTicket.Next(0, countTickets);
            }
            indicesOfTickets.Add(indexCurrentTicket);
            return _tickets.GetAll()[indexCurrentTicket];            
        }

        private User GetCurrentBuyer(int countUsers)
        {
            Random indexRandomBuyer = new Random();
            int indexCurrentBuyer = indexRandomBuyer.Next(0, countUsers);
            return _users.GetAllUsers()[indexCurrentBuyer];
        }

        public async void CreateNewOrder(Ticket ticket, string userName)
        {
            string status = StatusOrder.Waiting_Confirmation;
            User buyer = _users.FindByUserNameAsync(userName).Result;
            Order order = new Order(){Ticket = ticket, Status = status, Buyer = buyer, TrakNO = "" };
            Add(order);
        }

        public void Add(Order order)
        {
            _db.Orders.Add(order);
            _db.SaveChanges();
        }

        public void Update(Order order)
        {
            _db.Orders.Update(order);
            _db.SaveChanges();
        }

        public List<Order> GetAll()
        {
            return _db.Orders
                   .Include(o => o.Ticket) 
                     .ThenInclude(t => t.Event)
                        .ThenInclude(e => e.Venue)
                            .ThenInclude(v => v.City)
                   .Include(o => o.Buyer)
                   .ToList();
        }

        public Order FindByID(int orderId)
        {
            return GetAll().FirstOrDefault(o => o.Id == orderId);
        }

        public List<Order> FindByBuyerID(string buyerId)
        {
            return GetAll().FindAll(o => o.Buyer.Id == buyerId);
        }
    }
}
