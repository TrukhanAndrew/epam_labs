﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Trukhan.TicketReSailing.Business.Interfaces;
using Trukhan.TicketReSailing.Business.Models;

namespace Trukhan.TicketReSailing.Business.Repositories
{
    public class LocalizationRepository: ILocalizationRepository
    {
        private ApplicationContext _db;

        public LocalizationRepository(ApplicationContext db)
        {
            _db = db;
            AddLocalizationsIfNoneExist();
        }

        private void AddLocalizationsIfNoneExist()
        {
            if (_db.Localizations.Count() == 0)
            {
                Localization localization = new Localization { Name = "en", Value = "en-US"};
                Add(localization);

                localization = new Localization { Name = "ru", Value = "ru-RU" };
                Add(localization);

                localization = new Localization { Name = "be", Value = "be-BY" };
                Add(localization);
            }
        }

        public List<string> GetLocalizationsName ()
        {
            return (from l in _db.Localizations.ToList()
                   select l.Name).ToList();

        }

        private async void FindByNameAsync (string name, Localization localization)
        {
           localization = await _db.Localizations.FirstOrDefaultAsync(l => l.Name == name);
        }

        
        public async Task<Localization> FindByIdAsync (int localizationID)
        {
          Localization localization = await _db.Localizations.FirstOrDefaultAsync(l => l.Id == localizationID);
          return localization;
        }
       

        public Localization FindByName(string name)
        {
            Localization localization = _db.Localizations.FirstOrDefault(l => l.Name == name);
            //Localization localization = new Localization();
            //FindByNameAsync(name, localization);
            return localization;
        }

        public void Add (Localization localization)
        {
            _db.Localizations.Add(localization);
            _db.SaveChanges();
        }

    }
}
