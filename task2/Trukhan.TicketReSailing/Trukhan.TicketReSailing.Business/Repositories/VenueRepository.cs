﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Trukhan.TicketReSailing.Business.Interfaces;
using Trukhan.TicketReSailing.Business.Models;

namespace Trukhan.TicketReSailing.Business.Repositories
{
    public class VenueRepository: IVenueRepository
    {
        private ApplicationContext _db;
        private readonly ICityRepository _cities;

        public VenueRepository(ICityRepository cities, ApplicationContext db)
        {
            _db = db;
            _cities = cities;
            AddVenuesIfNoneExist();
        }

        private void AddVenuesIfNoneExist()
        {
            if (_db.Venues.Count() == 0)
            {
                List<City> cities = _cities.GetAll();
                int idCity = 0;

                string adress = "Boryasenko st, 5";
                string name = "Concert Holl";
                Venue venue = new Venue() { City = cities[idCity], Address = adress, Name = name };
                Add(venue);

                adress = "Lange st, 2";
                name = "Socially Cultural Center";
                venue = new Venue() { City = cities[idCity], Address = adress, Name = name };
                Add(venue);
                idCity++;

                adress = "Victory st, 10";
                name = "Drama Theatre";
                venue = new Venue() { City = cities[idCity], Address = adress, Name = name };
                Add(venue);

                adress = "Independence st, 103";
                name = "Circus";
                venue = new Venue() { City = cities[idCity], Address = adress, Name = name };
                Add(venue);
                idCity++;

                adress = "Lenin Square, 1";
                name = "The House of Cinema";
                venue = new Venue() { City = cities[idCity], Address = adress, Name = name };
                Add(venue);

                adress = "Octyabrskaya Square, 1";
                name = "Palace of the republic";
                venue = new Venue() { City = cities[idCity], Address = adress, Name = name };
                Add(venue);
                idCity++;

                adress = "Theatre Square, 1";
                name = "Polish National Opera";
                venue = new Venue() { City = cities[idCity], Address = adress, Name = name };
                Add(venue);

                adress = "Gold st, 9";
                name = "Palladium";
                venue = new Venue() { City = cities[idCity], Address = adress, Name = name };
                Add(venue);
                idCity++;

                adress = "Kensington Gore";
                name = "Royal Albert Hall";
                venue = new Venue() { City = cities[idCity], Address = adress, Name = name };
                Add(venue);

                adress = "Compton Terrace";
                name = "Union Chapel";
                venue = new Venue() { City = cities[idCity], Address = adress, Name = name };
                Add(venue);
                idCity++;

                adress = "Popovicha st, 2";
                name = "Filarmony";
                venue = new Venue() { City = cities[idCity], Address = adress, Name = name };
                Add(venue);

                adress = "Marks st, 8";
                name = "Palace of culture";
                venue = new Venue() { City = cities[idCity], Address = adress, Name = name };
                Add(venue);
                idCity++;
            }
        }

        public void Add(Venue venue)
        {
            _db.Venues.Add(venue);
            _db.SaveChanges();
        }

        public List<Venue> GetAll()
        {
            return _db.Venues
                    .Include(v => v.City).ToList(); ;
        }

        public Venue FindById(int venueId)
        {
            return _db.Venues
                    .Include(v => v.City)
                    .FirstOrDefault(v => v.Id == venueId);
        }

        public Venue FindByName(string venueName)
        {
            return _db.Venues.FirstOrDefault(v => v.Name == venueName);
        }

        public List<Venue> FindByCityID (int cityID)
        {
            return _db.Venues.ToList().FindAll(v => v.City.Id == cityID);
        }

        public void Delete(Venue venue)
        {
            _db.Venues.Remove(venue);
            _db.SaveChanges();
        }

        public void Update(Venue venue)
        {
            _db.Venues.Update(venue);
            _db.SaveChanges();
        }
    }
}