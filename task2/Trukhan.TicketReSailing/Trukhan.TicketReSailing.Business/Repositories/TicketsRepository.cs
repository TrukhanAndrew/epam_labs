﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Trukhan.TicketReSailing.Business.Interfaces;
using Trukhan.TicketReSailing.Business.Models;

namespace Trukhan.TicketReSailing.Business.Repositories
{
    public class TicketsRepository : ITicketsRepository
    {
        private ApplicationContext _db;
        private readonly IUserRepository _users;
        private readonly IEventRepository _events;

        public TicketsRepository (ApplicationContext db, IUserRepository users, IEventRepository events)
        {
            _db = db;
            _users = users;
            _events = events;
            AddTicketsIfNoneExist();
        }

        private void AddTicketsIfNoneExist()
        {
            if (_db.Tickets.Count() == 0)
            {
                List<Event> events = _events.GetAll();
                List<User> users = _users.GetAllUsers();
                string status = StatusTicket.Selling;

                double price = 123;
                Ticket ticket = new Ticket() { Event = GetCurentEvent(events), Seller = GetCurentSeller(users), Price = price, Status = status};
                Add(ticket);

                price = 150;
                ticket = new Ticket() { Event = GetCurentEvent(events), Seller = GetCurentSeller(users), Price = price, Status = status };
                Add(ticket);

                price = 200;
                ticket = new Ticket() { Event = GetCurentEvent(events), Seller = GetCurentSeller(users), Price = price, Status = status };
                Add(ticket);

                price = 270;
                ticket = new Ticket() { Event = GetCurentEvent(events), Seller = GetCurentSeller(users), Price = price, Status = status };
                Add(ticket);

                price = 100;
                ticket = new Ticket() { Event = GetCurentEvent(events), Seller = GetCurentSeller(users), Price = price, Status = status };
                Add(ticket);

                price = 220;
                ticket = new Ticket() { Event = GetCurentEvent(events), Seller = GetCurentSeller(users), Price = price, Status = status };
                Add(ticket);

                price = 1050;
                ticket = new Ticket() { Event = GetCurentEvent(events), Seller = GetCurentSeller(users), Price = price, Status = status };
                Add(ticket);

                price = 350;
                ticket = new Ticket() { Event = GetCurentEvent(events), Seller = GetCurentSeller(users), Price = price, Status = status };
                Add(ticket);

                price = 900;
                ticket = new Ticket() { Event = GetCurentEvent(events), Seller = GetCurentSeller(users), Price = price, Status = status };
                Add(ticket);

                price = 180;
                ticket = new Ticket() { Event = GetCurentEvent(events), Seller = GetCurentSeller(users), Price = price, Status = status };
                Add(ticket);

                price = 500;
                ticket = new Ticket() { Event = GetCurentEvent(events), Seller = GetCurentSeller(users), Price = price, Status = status };
                Add(ticket);
            }
        }

        private Event GetCurentEvent(List<Event> events)
        {
            int countEvents = events.Count();
            Random randomIndexEvent = new Random();
            int indexCurentEvent = randomIndexEvent.Next(0, countEvents);
            return events[indexCurentEvent];
        }

        private User GetCurentSeller (List<User> users)
        {
            int countUsers = users.Count(); ;
            Random randomIndexSeller = new Random();
            int indexCurentSeller = randomIndexSeller.Next(0, countUsers);
            return users[indexCurentSeller];
        }

        public void Add(Ticket ticket)
        {
            _db.Tickets.Add(ticket);
            _db.SaveChanges();
        }

        public void Update(Ticket ticket)
        {
            _db.Tickets.Update(ticket);
            _db.SaveChanges();
        }

        public List<Ticket> GetAll()
        {
            return _db.Tickets
                    .Include(t => t.Seller)
                    .Include(t => t.Event)
                    .ToList();
        }

        public Ticket FindByID (int ticketId)
        {
            return _db.Tickets.FirstOrDefault(t => t.Id == ticketId);
        }

        public List<Ticket> FindBySellerName (string name)
        {
            return GetAll().FindAll(t => t.Seller.UserName == name);
        }

        public List<Ticket> FindByEventID(int eventId)
        {
            return GetAll().FindAll(t => (t.Event.Id == eventId) && (t.Status == StatusTicket.Selling));
        }

    }
}
