﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Trukhan.TicketReSailing.Business.Interfaces;
using Trukhan.TicketReSailing.Business.Models;

namespace Trukhan.TicketReSailing.Business.Repositories
{
    public class LoginRepository: ILoginRepository
    {
        private static List<Login> logins;
        int i;

        public LoginRepository(IUserRepository userRepository)
        {
            logins = new List<Login>();
            List<User> users = userRepository.GetAllUsers();

            Login login = new Login(users[i], "Admin", "Admin");
            Add(login);

            login = new Login(users[i], "User", "User");
            Add(login);

            login = new Login(users[i], "User1", "User1");
            Add(login);
        }

        public List<Login> GetAll()
        {
            return logins;
        }

        public Login Find (string nameValue, string passwordValue)
         {
            return (from log in logins
                   where (log.Name == nameValue) && (log.Password == passwordValue)
                   select log).First();
        }

        public Login Find (string nameValue)
        {
            return (from log in logins
                    where log.Name == nameValue
                    select log).First();
        }

        public void Add(Login login)
        {
            logins.Add(login);
            i++;
        }

    }
}
