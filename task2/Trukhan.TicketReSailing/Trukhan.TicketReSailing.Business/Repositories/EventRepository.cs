﻿using System;
using System.Collections.Generic;
using System.Linq;
using Trukhan.TicketReSailing.Business.Models;
using Trukhan.TicketReSailing.Business.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Trukhan.TicketReSailing.Business.Repositories
{
    public class EventRepository: IEventRepository
    {
        private ApplicationContext _db;
        private readonly IVenueRepository _venues;

        public EventRepository(IVenueRepository venues, ApplicationContext db)
        {
            _db = db;
            _venues = venues;
            AddEventsIfNoneExist();
        }        

        public void AddEventsIfNoneExist()
        {
            if (_db.Events.Count() == 0)
            {
                List<Venue> venues = _venues.GetAll();

                DateTime dT = new DateTime(2017, 1, 11);
                string nameEvent = "Exhibition of unique pictures of Renaissance";
                string banner = "Renaissance.jpg";
                string descriptionEvent = "At an exhibition are presented earlier not exhibited pictures of unknown painter of Renaissance";
                Event ev = new Event { Name = nameEvent, Date = dT, Venue = venues[0], Banner = banner, Description = descriptionEvent };
                Add(ev);

                dT = new DateTime(2017, 1, 14);
                nameEvent = "Modern Art";
                banner = "Modern Art.jpg";
                descriptionEvent = "Modern Art. Style & Inspiration";
                ev = new Event { Name = nameEvent, Date = dT, Venue = venues[1], Banner = banner, Description = descriptionEvent };
                Add(ev);

                dT = new DateTime(2017, 3, 19);
                nameEvent = "Romeo and Juliete";
                banner = "Romeo and Juliette.jpg";
                descriptionEvent = "History of Love";
                ev = new Event { Name = nameEvent, Date = dT, Venue = venues[2], Banner = banner, Description = descriptionEvent };
                Add(ev);

                dT = new DateTime(2017, 3, 22);
                nameEvent = "ABBA";
                banner = "ABBA.jpg";
                descriptionEvent = "The only concert in Belarus";
                ev = new Event { Name = nameEvent, Date = dT, Venue = venues[3], Banner = banner, Description = descriptionEvent };
                Add(ev);

                dT = new DateTime(2017, 4, 2);
                nameEvent = "Avatar";
                banner = "Avatar.jpg";
                descriptionEvent = "Fantastic movie";
                ev = new Event { Name = nameEvent, Date = dT, Venue = venues[4], Banner = banner, Description = descriptionEvent };
                Add(ev);               

                dT = new DateTime(2017, 4, 13);
                nameEvent = "Metallica";
                banner = "Metallica.jpg";
                descriptionEvent = "The only concert in Belarus";
                ev = new Event { Name = nameEvent, Date = dT, Venue = venues[5], Banner = banner, Description = descriptionEvent };
                Add(ev);

                dT = new DateTime(2017, 4, 16);
                nameEvent = "Metallica";
                banner = "Metallica.jpg";
                descriptionEvent = "The only concert in Poland";
                ev = new Event { Name = nameEvent, Date = dT, Venue = venues[6], Banner = banner, Description = descriptionEvent };
                Add(ev);

                dT = new DateTime(2017, 6, 28);
                nameEvent = "Zdob si Zdub";
                banner = "Zdob si Zdub.jpg";
                descriptionEvent = "Powerful concert";
                ev = new Event { Name = nameEvent, Date = dT, Venue = venues[1], Banner = banner, Description = descriptionEvent };
                Add(ev);

                dT = new DateTime(2017, 6, 29);
                nameEvent = "Metallica";
                banner = "Metallica.jpg";
                descriptionEvent = "The only concert in Great Britain";
                ev = new Event { Name = nameEvent, Date = dT, Venue = venues[2], Banner = banner, Description = descriptionEvent };
                Add(ev);

                dT = new DateTime(2017, 8, 10);
                nameEvent = "Bjork";
                banner = "Bjork.jpg";
                descriptionEvent = "Unusual and fascinating show from Bjork";
                ev = new Event { Name = nameEvent, Date = dT, Venue = venues[3], Banner = banner, Description = descriptionEvent };
                Add(ev);

                dT = new DateTime(2017, 8, 19);
                nameEvent = "ABBA";
                banner = "ABBA.jpg";
                descriptionEvent = "The second only concert in Belarus";
                ev = new Event { Name = nameEvent, Date = dT, Venue = venues[4], Banner = banner, Description = descriptionEvent };
                Add(ev);                

                dT = new DateTime(2017, 9, 2);
                nameEvent = "Bryan Adams: The Best";
                banner = "Bryan Adams.jpg";
                descriptionEvent = "Within the world tour";
                ev = new Event { Name = nameEvent, Date = dT, Venue = venues[5], Banner = banner, Description = descriptionEvent };
                Add(ev);

                dT = new DateTime(2017, 11, 5);
                nameEvent = "Circus du Soleil";
                banner = "amaluna-show.jpg";
                descriptionEvent = "The new program AmaLuna";
                ev = new Event { Name = nameEvent, Date = dT, Venue = venues[6], Banner = banner, Description = descriptionEvent };
                Add(ev);
            }
        }

        public List<Event> GetAll()
        {
                return _db.Events
                    .Include(e => e.Venue)
                      .ThenInclude(v => v.City).ToList();
        }

        public Event FindById (int eventId)
        {
            return _db.Events.FirstOrDefault(e => e.Id == eventId);
        }

        public void Add(Event ev)
        {
            _db.Events.Add(ev);
            _db.SaveChanges();
        }

        public void Delete(Event ev)
        {
            _db.Events.Remove(ev);
            _db.SaveChanges();
        }

        public void Update(Event ev)
        {
            _db.Events.Update(ev);
            _db.SaveChanges();
        }
    }
}
