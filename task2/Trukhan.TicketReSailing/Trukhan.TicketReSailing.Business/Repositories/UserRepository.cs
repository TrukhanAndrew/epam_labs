﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Trukhan.TicketReSailing.Business.Interfaces;
using Trukhan.TicketReSailing.Business.Models;

namespace Trukhan.TicketReSailing.Business.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly UserManager <User> _userManager;
        private readonly RoleManager <IdentityRole> _roleManager;
        private readonly ILocalizationRepository _localizationRepository;
        private readonly SignInManager<User> _signInManager;
        private ApplicationContext _db;

        private string _login;
        private string _firsName;
        private string _lastName;
        private string _adress;
        private string _phoneNumber;
        private string _email;
        private string _role;
        private string _password;
        private Localization _localization;

        public UserRepository(ILocalizationRepository localizationRepository, UserManager<User> userManager, RoleManager<IdentityRole> roleManager, SignInManager<User> signInManager/*, ApplicationContext db*/)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _localizationRepository = localizationRepository;
            _signInManager = signInManager;

            AddRolesAsync();
            AddUsersIfNoneExist();
        }

        private void AddUsersIfNoneExist()
        {
            if (_userManager.Users.Count() == 0)
            {
                _login = "admin";
                _firsName = "Vladimir";
                _lastName = "Petrov";
                _role = "admin";
                _password = "admin";
                _adress = "Republic of Belarus, Grodno, Melega st.5 - 12";
                _email = "ret@tut.by";
                _phoneNumber = "(029)564 - 12 - 12";
                _localization = _localizationRepository.FindByName("en");
                User user = new User { UserName = _login, FirstName = _firsName, LastName = _lastName, Localization = _localization, Adress = _adress, Email = _email, PhoneNumber = _phoneNumber };
                var result = AddUsersAsync(user, _password, _role);

                _login = "user";
                _firsName = "Bob";
                _lastName = "Fly";
                _password = "user";
                _role = "user";
                _adress = "England, London, Baker st.3";
                _email = "tt@yandex.ru";
                _phoneNumber = "(044)78-71-23-45-67";
                _localization = _localizationRepository.FindByName("en");
                user = new User { UserName = _login, FirstName = _firsName, LastName = _lastName, Localization = _localization, Adress = _adress, Email = _email, PhoneNumber = _phoneNumber };
                result = AddUsersAsync(user, _password, _role);

                _login = "user1";
                _firsName = "Janek";
                _lastName = "Dubka";
                _password = "user1";
                _role = "user";
                _adress = "Warsaw, Poland,  Mysliwiecka st.9 -11";
                _email = "gfhklgd@gmail.com";
                _phoneNumber = "(022)622-91-10";
                _localization = _localizationRepository.FindByName("en");
                user = new User { UserName = _login, FirstName = _firsName, LastName = _lastName, Localization = _localization, Adress = _adress, Email = _email, PhoneNumber = _phoneNumber };
                result = AddUsersAsync(user, _password, _role);
            }
        }

        private async void AddRolesAsync()
        {
            if (_roleManager.FindByNameAsync("admin").Result == null)
            {
                _roleManager.CreateAsync(new IdentityRole("admin")).RunSynchronously();
            }
            if (_roleManager.FindByNameAsync("user").Result == null)
            {
               _roleManager.CreateAsync(new IdentityRole("user")).RunSynchronously();
            }
        }

        public async Task<IdentityResult> AddUsersAsync (User user, string password, string role)
        {
            IdentityResult result = _userManager.CreateAsync(user, password).Result;
            if (result.Succeeded)
            {
                var resultAddToRole = _userManager.AddToRoleAsync(user, role).Result;
                var resultSignIn = _signInManager.SignInAsync(user, false);
                resultSignIn.Wait();
            }
            return result;
        }

        public void AddUsers(User user, string role)
        {
            _db.Users.Add(user);
            _db.Roles.Add(new IdentityRole(role));
            _db.SaveChanges();

            _db.UserRoles.Add (new IdentityUserRole<string>()
            {
                RoleId = _db.Roles.FirstOrDefault(r => r.Name == role).Id,
                UserId = _db.Users.FirstOrDefault(u => u.UserName == user.UserName).Id
            });
            _db.SaveChanges();
        }

        public List<User> GetAllUsers()
        {
            List<User> users = _userManager.Users.ToList();
            GetLocalization(users);
            return users;
        }

        public List<IdentityRole> GetAllRoles()
        {
            return _roleManager.Roles.ToList();
        }
        
        public bool AddToRole (User user, string role)
        {
            IdentityResult result = _userManager.AddToRoleAsync(user, role).Result;
            return (result.Succeeded);
        }

        public bool RemoveRole (User user, string role)
        {
            IdentityResult result = _userManager.RemoveFromRoleAsync(user, role).Result;
            return (result.Succeeded);
        }
    
        public async Task<User> FindByUserNameAsync (string userName)
        {
            var user = await _userManager.FindByNameAsync (userName);
            user.Localization = await _localizationRepository.FindByIdAsync(user.LocalizationId);
            return user;
        }

        public async Task<User> FindByUserIdAsync (string userId)
        {
            var user = await _userManager.FindByIdAsync (userId);
            user.Localization = await _localizationRepository.FindByIdAsync(user.LocalizationId);
            return user; 
        }

        private async void GetLocalization(List<User> users)
        {
            foreach (var user in users)
            {
                user.Localization = _localizationRepository.FindByIdAsync(user.LocalizationId).Result;
            }

        }

        public bool IsInRole(User user, string role)
        {
            bool r = _userManager.IsInRoleAsync(user, role).Result;
            return r;
        }
        
    }
}
