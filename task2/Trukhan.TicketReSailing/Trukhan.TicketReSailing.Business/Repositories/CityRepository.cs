﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Trukhan.TicketReSailing.Business.Interfaces;
using Trukhan.TicketReSailing.Business.Models;

namespace Trukhan.TicketReSailing.Business.Repositories
{
    public class CityRepository: ICityRepository
    {
        private ApplicationContext _db;

        public CityRepository(ApplicationContext db)
        {
            _db = db;
            AddCitiesIfNoneExist();            
        }

        public void AddCitiesIfNoneExist()
        {
            if (!_db.Cities.Any())
            {
                City city = new City { Name = "Homiel" };
                Add(city);

                city = new City { Name = "Brest" };
                Add(city);

                city = new City { Name = "Minsk" };
                Add(city);

                city = new City { Name = "Warsaw" };
                Add(city);

                city = new City { Name = "London" };
                Add(city);

                city = new City { Name = "Grodno" };
                Add(city);
            }
        }
        

        public void Add(City city)
        {
            _db.Cities.Add(city);
            _db.SaveChanges();
        }

        public List<City> GetAll()
        {
            return _db.Cities.ToList();
        }

        public City FindById(int cityId)
        {
            return _db.Cities.FirstOrDefault(c => c.Id == cityId);
        }

        public City FindByName(string cityName)
        {
            return _db.Cities.FirstOrDefault(c => c.Name == cityName);
        }

        public void Delete(City city)
        {
            _db.Cities.Remove(city);
            _db.SaveChanges();
        }

        public void Update(City city)
        {
            _db.Cities.Update(city);
            _db.SaveChanges();
        }
    }
}
