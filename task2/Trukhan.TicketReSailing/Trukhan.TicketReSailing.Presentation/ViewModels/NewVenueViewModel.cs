﻿using System.ComponentModel.DataAnnotations;
using Trukhan.TicketReSailing.Business.Models;

namespace Trukhan.TicketReSailing.Presentation.ViewModels
{
    public class NewVenueViewModel
    {
        [Required]
        [Display(Name = "Venue name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "City")]
        public string City { get; set; }
    }
}
