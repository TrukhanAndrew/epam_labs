﻿using System.ComponentModel.DataAnnotations;

namespace Trukhan.TicketReSailing.Presentation.ViewModels
{
    public class NewCityViewModel
    {
        [Required]
        [Display(Name = "City name")]
        public string Name { get; set; }
    }
}
