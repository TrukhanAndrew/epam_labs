﻿using System.ComponentModel.DataAnnotations;
using Trukhan.TicketReSailing.Business.Models;

namespace Trukhan.TicketReSailing.Presentation.ViewModels
{
    public class NewEventViewModel
    {
        [Required]
        [Display(Name="Event Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        public string Date { get; set; }

        [Required]
        [Display(Name = "Venue")]
        public string Venue { get; set; }

        [Required]
        [Display(Name = "Banner")]
        public string Banner { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }
    }
}
