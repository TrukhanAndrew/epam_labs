﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Trukhan.TicketReSailing.Business.Models;
using Microsoft.Extensions.Localization;
using Trukhan.TicketReSailing.Business.Interfaces;
using Microsoft.AspNetCore.Authorization;
using System.Linq;

namespace Trukhan.TicketReSailing.Presentation.Controllers
{

    public class TicketController : Controller
    {
        public TicketController(IStringLocalizer<TicketController> localizer, ITicketsRepository ticketsRepository, IUserRepository users, IOrdersRepository ordersRepository)
        {
            _localizer = localizer;
            _ticketsRepository = ticketsRepository;
            _ordersRepository = ordersRepository;
            _users = users;
        }

        private IStringLocalizer<TicketController> _localizer;
        private ITicketsRepository _ticketsRepository;
        private IOrdersRepository _ordersRepository;
        private IUserRepository _users;

        [HttpGet]
        public IActionResult ListTickets(int eventId)
        {
            ViewBag.NamePage = _localizer["List tickets"];
            List<Ticket> ticketsEvent = _ticketsRepository.FindByEventID(eventId);
            return View(ticketsEvent);
        }

        [Authorize]
        public IActionResult GetMyTickets()
        {
            ViewBag.NamePage = _localizer["My tickets"];
            List<Ticket> myTickets = _ticketsRepository.FindBySellerName(User.Identity.Name);
            return View(myTickets);
        }

        [Authorize]
        public IActionResult Confirm(int ticketId)
        {
            Ticket ticketConfirm = _ticketsRepository.FindByID(ticketId);
            if (ticketConfirm != null)
            {
                ticketConfirm.Status = StatusTicket.Waiting_Payment;
                Order order = _ordersRepository
                    .GetAll()
                    .FirstOrDefault(o => o.Ticket.Id == ticketId);
                order.Status = StatusOrder.Confirmed;
                _ordersRepository.Update(order);
                _ticketsRepository.Update(ticketConfirm);
            }
            return RedirectToAction("GetMyTickets");
        }

        [Authorize]
        public IActionResult Reject(int ticketId, string messageReject)
        {
            Ticket ticketReject = _ticketsRepository.FindByID(ticketId);
            if (ticketReject != null)
            {
                ticketReject.Status = StatusTicket.Selling;
                Order order = _ordersRepository
                    .GetAll()
                    .FirstOrDefault(o => o.Ticket.Id == ticketId);
                order.Status = StatusOrder.Rejected;
                order.MessageReject = messageReject;

                _ordersRepository.Update(order);
                _ticketsRepository.Update(ticketReject);
            }
            return RedirectToAction("GetMyTickets");
        }

        [Authorize]
        [HttpPost]
        public IActionResult ToOrder(int ticketId, int evId)
        {
            Ticket ticket = _ticketsRepository.FindByID(ticketId);
            ticket.Status = StatusTicket.Waiting_Confirmation;
            _ticketsRepository.Update(ticket);
            _ordersRepository.CreateNewOrder(ticket, User.Identity.Name);
            return RedirectToAction("ListTickets", new { eventId = evId});
        }
    }
}
