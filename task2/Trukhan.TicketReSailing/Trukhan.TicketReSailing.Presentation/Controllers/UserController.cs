﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Authorization;
using Trukhan.TicketReSailing.Business.Interfaces;
using Trukhan.TicketReSailing.Business.Models;

namespace Trukhan.TicketReSailing.Presentation.Controllers
{

    public class UserController : Controller
    {
        public UserController(IStringLocalizer<UserController> localizer, IUserRepository userRepostory, ILoginRepository loginRepository)
        {
            _localizer = localizer;
            _userRepostory = userRepostory;
            _loginRepository = loginRepository;
        }

        private IStringLocalizer<UserController> _localizer;
        public IUserRepository _userRepostory;
        public ILoginRepository _loginRepository;

        [Authorize]
        public async Task<IActionResult> GetUserInfo()
        {           
             ViewBag.NamePage = _localizer["Info about user"];
             User user = await _userRepostory.FindByUserNameAsync(User.Identity.Name);
             return View (user);
            
        }
    }
}
