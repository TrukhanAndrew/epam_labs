﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Trukhan.TicketReSailing.Business.Models;
using Trukhan.TicketReSailing.Business.Repositories;
using System.Security.Claims;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Trukhan.TicketReSailing.Business.Interfaces;
using Microsoft.AspNetCore.Identity;
using Trukhan.TicketReSailing.Presentation.ViewModels;
using System;
using System.Linq;
using Microsoft.Extensions.Localization;

namespace Trukhan.TicketReSailing.Presentation.Controllers
{   
    [Authorize(Roles="admin")]
    public class AdminController : Controller
    {

        public AdminController(IEventRepository eventRepository, IVenueRepository venueRepository, ICityRepository cityRepository, IStringLocalizer<AdminController> localizer)
        {
            _eventRepository = eventRepository;
            _venueRepository= venueRepository;
            _cityRepository= cityRepository;
		    _localizer = localizer;
        }

        private IEventRepository _eventRepository;
        private IVenueRepository _venueRepository;
        private ICityRepository _cityRepository;
	    private IStringLocalizer<AdminController> _localizer;

        [HttpGet]
        public IActionResult Index (int id)
        {
            switch (id)
            {
                case 1: 
                    ViewBag.NamePage = _localizer["EditingCities"];
                    return View("~/Views/ListsEntities/ViewCities.cshtml", _cityRepository);
                case 2: 
                    ViewBag.NamePage = _localizer["EditingVenues"];
                    return View("~/Views/ListsEntities/ViewVenues.cshtml", _venueRepository);
                default: 
                    ViewBag.NamePage = _localizer["EditingEvents"];
                    return View("~/Views/ListsEntities/ViewEvents.cshtml", _eventRepository);
            }
        }

        public IActionResult Create (int idEntity)
        {
            switch (idEntity)
            {
                case 1: return View("~/Views/EditingListEntities/ViewCreateCity.cshtml");
                case 2:
                    ViewBag.Cities = _cityRepository.GetAll();
                    return View("~/Views/EditingListEntities/ViewCreateVenue.cshtml");

                default:
                    ViewBag.Venues = _venueRepository.GetAll(); 
                    return View("~/Views/EditingListEntities/ViewCreateEvent.cshtml");
            }
        }

        public IActionResult CreateCity(NewCityViewModel model, int idEntity)
        {
            if (ModelState.IsValid)
            {
                City city = new City{Name = model.Name};
                _cityRepository.Add(city);
            }
            else
            {
                return RedirectToAction("Create", new { idEntity = idEntity });
            }
            return RedirectToAction("Index", new { id = idEntity });
        }

        public IActionResult CreateVenue(NewVenueViewModel model, int idEntity, int cityId)
        {
            if (ModelState.IsValid)
            {   
                City city = _cityRepository.FindById(cityId);
                Venue venue = new Venue 
                { 
                    Name = model.Name,
                    Address = model.Address,
                    City = city,
                };
                _venueRepository.Add(venue);
            }
            else
            {
                return RedirectToAction("Create", new { idEntity = idEntity });
            }
            return RedirectToAction("Index", new { id = idEntity });
        }

        public IActionResult CreateEvent (NewEventViewModel model, int idEntity, int venueId)
        {
            if (ModelState.IsValid)
            {
                Venue venue = _venueRepository.FindById(venueId);
                Event ev = new Event
                {
                    Name = model.Name,
                    Date = DateTime.Parse(model.Date),
                    Venue = venue,
                    Banner = model.Banner,
                    Description = model.Description,
                };
                _eventRepository.Add(ev);
            }
            else
            {
                return RedirectToAction("Create", new { idEntity = idEntity });
            }
            return RedirectToAction("Index", new { id = idEntity });
        }

        [HttpGet]
        public IActionResult Update(int id, int idEntity)
        {
            switch (idEntity)
            {
                case 1:
                    City city = _cityRepository.FindById(id);
                    if (city != null)
                        return View("~/Views/EditingListEntities/ViewUpdateCity.cshtml", city);
                    break;
                case 2:
                    Venue venue = _venueRepository.FindById(id);
                    if (venue != null)
                        ViewBag.Cities = _cityRepository.GetAll();
                        return View("~/Views/EditingListEntities/ViewUpdateVenue.cshtml", venue);
                    break;
                case 3: 
                    Event ev = _eventRepository.FindById(id);
                    if (ev != null)
                        ViewBag.Venues = _venueRepository.GetAll();
                        return View("~/Views/EditingListEntities/ViewUpdateEvent.cshtml", ev);
                    break;
            }
            return RedirectToAction("Index", new { id = idEntity });
        }

        [HttpPost]
        public IActionResult UpdateCity(City city)
        {
            _cityRepository.Update(city);            
            return RedirectToAction("Index", new { id = 1 });
        }

        [HttpPost]
        public IActionResult UpdateVenue(Venue venue, int cityId)
        {
            City city = _cityRepository.FindById(cityId);
            venue.City = city;
            _venueRepository.Update(venue);
            return RedirectToAction("Index", new { id = 2 });
        }

        [HttpPost]
        public IActionResult UpdateEvent(Event ev, int venueId)
        {
            Venue venue = _venueRepository.FindById(venueId);
            ev.Venue = venue;
            _eventRepository.Update(ev);
            return RedirectToAction("Index", new { id = 3 });
        }

        public IActionResult Delete(int id, int idEntity)
        {
            switch(idEntity)    
            {
                case 1:
                    City city = _cityRepository.FindById(id);
                    if (city != null)
                        _cityRepository.Delete(city);
                    break;
                case 2:
                    Venue venue = _venueRepository.FindById(id);
                    if (venue != null)
                        _venueRepository.Delete(venue);
                    break;
                case 3:
                    Event ev = _eventRepository.FindById(id);
                    if (ev != null)
                        _eventRepository.Delete(ev);
                    break;
            }
            return RedirectToAction("Index", new {id = idEntity});
        }   
        
 
    }
}

