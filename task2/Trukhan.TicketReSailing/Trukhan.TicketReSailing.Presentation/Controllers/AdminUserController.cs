﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Trukhan.TicketReSailing.Business.Models;
using Trukhan.TicketReSailing.Business.Repositories;
using System.Security.Claims;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Trukhan.TicketReSailing.Business.Interfaces;
using Microsoft.AspNetCore.Identity;
using Trukhan.TicketReSailing.Presentation.ViewModels;
using System;
using System.Linq;
using Microsoft.Extensions.Localization;

namespace Trukhan.TicketReSailing.Presentation.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdminUserController : Controller
    {

        public AdminUserController(IUserRepository userRepository, IStringLocalizer<AdminUserController> localizer)
        {
            _userRepository = userRepository;
            _localizer = localizer;
        }

        private IUserRepository _userRepository;
        private IStringLocalizer<AdminUserController> _localizer;

        [HttpGet]
        public IActionResult ViewUsers()
        {
            List<User> users = _userRepository.GetAllUsers();
            return View(users);
        }


        public IActionResult AddRole(string userId, string userRole)
        {
            User user = _userRepository.FindByUserIdAsync(userId).Result;
            bool result = _userRepository.AddToRole(user, userRole);
            if (result)
            {
                ViewData["Message"] = String.Format("Роль {0} для пользователя {1} успешно добавлена", userRole, user.UserName);
            }
            else
            {
                ViewData["Message"] = String.Format("Роль {0} для пользователя {1} не была добавлена", userRole, user.UserName);
            }
            return RedirectToAction("ViewUsers");
        }

        public IActionResult RemoveRole(string userId, string userRole)
        {
            User user = _userRepository.FindByUserIdAsync(userId).Result;
            bool result = _userRepository.RemoveRole(user, userRole);
            if (result)
            {
                ViewBag.Message = String.Format("Роль {0} у пользователя {1} успешно удалена", userRole, user.UserName);
            }
            else
            {
                ViewBag.Message = String.Format("Роль {0} у пользователя {1} не была удалена", userRole, user.UserName);
            }
            return RedirectToAction("ViewUsers");
        }
    }
}

