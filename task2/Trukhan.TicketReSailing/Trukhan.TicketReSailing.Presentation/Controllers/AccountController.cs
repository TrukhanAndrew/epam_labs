﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Trukhan.TicketReSailing.Business.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Trukhan.TicketReSailing.Business.Interfaces;
using Microsoft.AspNetCore.Identity;
using Trukhan.TicketReSailing.Presentation.ViewModels;
using System;
using Microsoft.AspNetCore.Mvc.Rendering;


namespace Trukhan.TicketReSailing.Presentation.Controllers
{
    public class AccountController : Controller
    {
        
        public AccountController(IUserRepository userRepository, SignInManager<User> signInManager, ILocalizationRepository localizationRepository)
        {
            _userRepository = userRepository;
            _localizationRepository = localizationRepository;
            _signInManager = signInManager;
        }
        
        private IUserRepository _userRepository { get; set; }
        private ILocalizationRepository _localizationRepository { get; set; }
        private SignInManager<User> _signInManager {get; set;}

        [HttpGet]
        public IActionResult Login (string returnUrl)
        {
            return View(new LoginViewModel() { ReturnUrl = returnUrl});
        }

        [HttpPost]
        public async Task<IActionResult> Login (LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var resultLogin = await _signInManager.PasswordSignInAsync(model.Name, model.Password, model.RememberMe, false);

                 if (resultLogin.Succeeded)
                 {
                    if (!String.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                    {
                        User user = await _userRepository.FindByUserNameAsync(model.Name);
                        CookiesCulture.SetCookiesCulture(Response, user.Culture.Name);
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Неправильный логин и (или) пароль");
                }
            }
        return View(model);
      }

        [Authorize]
        public async Task<IActionResult> Logout()
        {
           await _signInManager.SignOutAsync();
           return RedirectToAction("Index", "Home");
        }   

        [HttpGet]
        public IActionResult Register()
        {
            IEnumerable<string> localization = _localizationRepository.GetLocalizationsName();
            ViewBag.Languages = new SelectList(localization);
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if(ModelState.IsValid)
            {
                User user = new User 
                            { 
                                UserName = model.Login, 
                                FirstName = model.FirstName, 
                                LastName = model.LastName,
                                Localization = _localizationRepository.FindByName(model.Language), 
                                Adress = model.Adress, 
                                PhoneNumber = model.PhoneNumber,
                                Email = model.Email
                            };

                string role = "user";

                var result = _userRepository.AddUsersAsync(user, model.Password, role).Result;
                
                if (result.Succeeded)
                {
                    CookiesCulture.SetCookiesCulture(Response, user.Culture.Name);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }

            return RedirectToAction("Register");
        }
    }
}
