﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Trukhan.TicketReSailing.Business.Models;
using Trukhan.TicketReSailing.Business.Repositories;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Authorization;
using Trukhan.TicketReSailing.Business.Interfaces;

namespace Trukhan.TicketReSailing.Presentation.Controllers
{
    

    public class OrderController : Controller
    {
        public OrderController(IStringLocalizer<OrderController> localizer, IOrdersRepository ordersRepository, IUserRepository userRepository)
        {
            _localizer = localizer;
            _ordersRepository = ordersRepository;
            _userRepository = userRepository;
        }

        private IStringLocalizer<OrderController> _localizer;
        private IOrdersRepository _ordersRepository;
        private IUserRepository _userRepository;

        [Authorize]
        public IActionResult GetMyOrders()
        {
            ViewBag.NamePage = _localizer["My orders"];
            string buyerId = _userRepository.FindByUserNameAsync(User.Identity.Name).Result.Id;
            List<Order> MyOrders = _ordersRepository.FindByBuyerID(buyerId);
            return View(MyOrders);
        }
    }
}
