﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System;
using System.Security.Claims;
using Trukhan.TicketReSailing.Business.Interfaces;
using Trukhan.TicketReSailing.Business.Models;


namespace Trukhan.TicketReSailing.Presentation.Controllers
{
    public class HomeController : Controller
    {
        public HomeController(IEventRepository eventRepository, IStringLocalizer<HomeController> localizer)
        {
            _eventRepository = eventRepository;
            _localizer = localizer;
        }

        public IEventRepository _eventRepository { get; set; }

        private IStringLocalizer<HomeController> _localizer;

        public IActionResult Index()
        {
            ViewBag.NamePage = _localizer["Events"];
            return View(_eventRepository);
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            CookiesCulture.SetCookiesCulture(Response, culture);
            return LocalRedirect(returnUrl);
        }
    }
}
